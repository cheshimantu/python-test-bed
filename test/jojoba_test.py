import allure
import pytest
import os


@pytest.mark.parametrize("test_param", "hello,my,friend".split(","))
@allure.title("test_allure_parametrized_test [{test_param}]")
def test_allure_parametrized_test(test_param):
    with allure.step("Step inside parametrized test"):
        pass
    with allure.step(f"Test parameter: {test_param}"):
        pass
